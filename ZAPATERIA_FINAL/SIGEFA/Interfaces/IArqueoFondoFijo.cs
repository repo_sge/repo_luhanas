﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface IArqueoFondoFijo
    {
        Boolean Insert(clsArqueoFondoFijo NuevoArqueo);

        Boolean insertDetalle(clsDetalleArqueFondoFijo det);
    }
}
