﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface IDiseño
    {
        Boolean Insert(clsDiseño mar);

        Boolean Update(clsDiseño mar);

        Boolean Delete(int Codmar);

        DataTable ListaDiseño();
    }
}
