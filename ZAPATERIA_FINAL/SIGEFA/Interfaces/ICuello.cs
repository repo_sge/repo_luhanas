﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using System.Data;

namespace SIGEFA.Interfaces
{
    interface ICuello
    {
        Boolean Insert(clsCuello mar);

        Boolean Update(clsCuello mar);

        Boolean Delete(int Codmar);

        DataTable ListaCuello();
    }
}
