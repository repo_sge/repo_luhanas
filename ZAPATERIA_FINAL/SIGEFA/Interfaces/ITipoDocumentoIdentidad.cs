﻿using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface ITipoDocumentoIdentidad
    {
        List<clsTipoDocumentoIdentidad> Listar_tipo_documento_identidad();
    }
}
