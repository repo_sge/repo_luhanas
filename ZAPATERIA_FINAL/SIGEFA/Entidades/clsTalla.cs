﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
   public class clsTalla
    {
        private Int32 icodTalla;
        private String sNombre;
        private String sCorto;
        private Int32 iCodLinea;
        private String sValor;
        private Int32 estado;
        private Int32 tipoTalla;

        public String Valor
        {
            get { return sValor; }
            set { sValor = value; }
        }

        public Int32 CodLinea
        {
            get { return iCodLinea; }
            set { iCodLinea = value; }
        }

        public String Corto
        {
            get { return sCorto; }
            set { sCorto = value; }
        }

        public String Nombre
        {
            get { return sNombre; }
            set { sNombre = value; }
        }

        public Int32 codTalla
        {
            get { return icodTalla; }
            set { icodTalla = value; }
        }

        public int TipoTalla { get => tipoTalla; set => tipoTalla = value; }
        public int Estado { get => estado; set => estado = value; }
    }
}
