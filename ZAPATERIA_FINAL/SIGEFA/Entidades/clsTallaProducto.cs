﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    public class clsTallaProducto
    {
        private Int32 idTalla_producto;
        private Int32 idTalla;
        private Int32 idProducto;
        private Decimal stock;
        private DateTime fechaRegistro;
        private Int32 idAlmacen;

        public int IdTalla_producto { get => idTalla_producto; set => idTalla_producto = value; }
        public int IdTalla { get => idTalla; set => idTalla = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public DateTime FechaRegistro { get => fechaRegistro; set => fechaRegistro = value; }
        public Decimal Stock { get => stock; set => stock = value; }
        public int IdAlmacen { get => idAlmacen; set => idAlmacen = value; }
    }
}
