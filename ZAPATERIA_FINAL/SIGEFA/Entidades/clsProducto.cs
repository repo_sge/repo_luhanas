﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace SIGEFA.Entidades
{
    public class clsProducto
    {

        #region propiedades

        private Int32 iCodProducto;        
        private Int32 iCodProductoAlmacen;
        private Int32 iCodAlmacen;
        private Int32 iCodUsuario;
        private Int32 iCodGrupo;
        private Int32 iCodLinea;
        private Int32 iCodFamilia;
        private Int32 iCodUnidadMedida;
        private Int32 iCodTipoArticulo;
        private Int32 iCodMarca;
        private Int32 iCodControlStock;
        private String sReferencia;
        private String sDescripcion;
        private Decimal dPrecioCatalogo;
        private Decimal dValorProm;
        private Decimal dPrecioProm;
        private Decimal dRecargo;
        private Decimal dValorVenta;
        private Decimal dPrecioVenta;
        private Decimal dPrecioVentaSoles;    
        private Decimal dPDescuento;
        private Decimal dMontoDscto;
        private Decimal dPrecioOferta;       
        private Decimal dMaximoDscto;
        private Decimal dStockActual;
        private Decimal dStockDisponible;
        private Decimal dStockMinimo;
        private Decimal dStockMaximo;
        private Decimal dStockReposicion;
        private Decimal dSoles;
        private Decimal dTotalIngresos;
        private Decimal dTotalSalidas;
        private Decimal dTotalSolesIngresos;
        private Decimal dTotalSolesSalidas;
        private Boolean bIgv;
        private Boolean bDetraccion;        
        private Boolean bEstado;
        private Boolean bOferta;
        private Boolean bConIGV;
        private Boolean bPrecioVariable;
        private DateTime dtUltimaModificacion;
        private DateTime dtFechaRegistro;
        private String sUnidaddescrip;
        private Decimal dComision;
        private Decimal dstockFuturo;
        private Decimal dstockPorRecibir;
        private Decimal dvalorPromsoles;
        private Decimal dMaxPorcDesc;
        private Int32 iPorllegar;
        private Int32 iPorAtender;
        private Int32 iPorCompletar;
        private Int32 icantidad;
        private Int32 icodSerieProducto;
        private Int32 icodSubTipoArticulo;
        private Int32 icodCorte;
        private Int32 icodModelo;
        private Int32 icodColorPrimario;
        private Int32 icodColorSecundario;
        private Int32 icodDetalle;
        private Int32 icodTaco;
        private Int32 icodFondo;
        private Int32 iTipoTalla;
        private String sCodigoBarra;
        private Image iFotoZapato;
        private String sControlStockDes;
        private Decimal dPorcPromediocompra;
        private Int32 iContieneTalla;
        //Josué Joshua León Yalta
        private String smarca;
        private String smodelo;
        private String scolorprimario;
        private String scolorsecundario;
        private String svalorTalla;
        private Int32 iCodtalla;
        private String iCodCompraProducto;
        private Int32 icodTipoCierre;
        private Int32 icodTemporada;
        private List<clsTalla> tallas;

        //boutique
        private Int32 icodCuello;
        private Int32 icodManga;
        private Int32 icodDiseño;
        private Int32 icodTipoDiseño;
        private Int32 iannio;

        private String sdetCodProducto1;

        public String detCodProducto1
        {
            get { return sdetCodProducto1; }
            set { sdetCodProducto1 = value; }
        }
        private String sdetCodProducto2;

        public String detCodProducto2
        {
            get { return sdetCodProducto2; }
            set { sdetCodProducto2 = value; }
        }
        private String sdetCodProducto3;

        public String detCodProducto3
        {
            get { return sdetCodProducto3; }
            set { sdetCodProducto3 = value; }
        }

        public Int32 Annio
        {
            get { return iannio; }
            set { iannio = value; }
        }

        public Int32 CodCuello
        {
            get { return icodCuello; }
            set { icodCuello = value; }
        }

        public Int32 CodManga
        {
            get { return icodManga; }
            set { icodManga = value; }
        }

        public Int32 CodDiseño
        {
            get { return icodDiseño; }
            set { icodDiseño = value; }
        }

        public Int32 CodTipoDiseño
        {
            get { return icodTipoDiseño; }
            set { icodTipoDiseño = value; }
        }

        public Int32 CodTemporada
        {
            get { return icodTemporada; }
            set { icodTemporada = value; }
        }

        public Int32 CodTipoCierre
        {
            get { return icodTipoCierre; }
            set { icodTipoCierre = value; }
        }

        public String CodCompraProducto
        {
            get { return iCodCompraProducto; }
            set { iCodCompraProducto = value; }
        }

        public Int32 Codtalla
        {
            get { return iCodtalla; }
            set { iCodtalla = value; }
        }

        public String valorTalla
        {
            get { return svalorTalla; }
            set { svalorTalla = value; }
        }


        
        public Decimal PorcPromediocompra
        {
            get { return dPorcPromediocompra; }
            set { dPorcPromediocompra = value; }
        }
        //varibale para saber a cuanto se ha comprado antes el producto
        private Decimal dPrecioCompra;

        public Decimal PrecioCompra
        {
            get { return dPrecioCompra; }
            set { dPrecioCompra = value; }
        }

        public String ControlStockDes
        {
            get { return sControlStockDes; }
            set { sControlStockDes = value; }
        }

        public Image FotoZapato
        {
            get { return iFotoZapato; }
            set { iFotoZapato = value; }
        }

        public String CodigoBarra
        {
            get { return sCodigoBarra; }
            set { sCodigoBarra = value; }
        }

        public Int32 codFondo
        {
            get { return icodFondo; }
            set { icodFondo = value; }
        }

        public Int32 codTaco
        {
            get { return icodTaco; }
            set { icodTaco = value; }
        }

        public Int32 codDetalle
        {
            get { return icodDetalle; }
            set { icodDetalle = value; }
        }
        public Int32 codColorSecundario
        {
            get { return icodColorSecundario; }
            set { icodColorSecundario = value; }
        }

        
        public Int32 codColorPrimario
        {
            get { return icodColorPrimario; }
            set { icodColorPrimario = value; }
        }

        public Int32 codModelo
        {
            get { return icodModelo; }
            set { icodModelo = value; }
        }

        public Int32 TipoTalla
        {
            get { return iTipoTalla; }
            set { iTipoTalla = value; }
        }

        public Int32 codCorte
        {
            get { return icodCorte; }
            set { icodCorte = value; }
        }


        public Int32 codSubTipoArticulo
        {
            get { return icodSubTipoArticulo; }
            set { icodSubTipoArticulo = value; }
        }


        public Int32 codSerieProducto
        {
            get { return icodSerieProducto; }
            set { icodSerieProducto = value; }
        }


        public Int32 Cantidad
        {
            get { return icantidad; }
            set { icantidad = value; }
        }
        public Int32 Porllegar
        {
            get { return iPorllegar; }
            set { iPorllegar = value; }
        }
        public Int32 PorAtender
        {
            get { return iPorAtender; }
            set { iPorAtender = value; }
        }
        public Int32 PorCompletar
        {
            get { return iPorCompletar; }
            set { iPorCompletar = value; }
        }
        public Decimal MaxPorcDesc
        {
            get { return dMaxPorcDesc; }
            set { dMaxPorcDesc = value; }
        }

        public Decimal StockPorRecibir
        {
            get { return dstockPorRecibir; }
            set { dstockPorRecibir = value; }
        }

        public Decimal StockFuturo
        {
            get { return dstockFuturo; }
            set { dstockFuturo = value; }
        }
        public Int32 CodProductoAlmacen
        {
            get { return iCodProductoAlmacen; }
            set { iCodProductoAlmacen = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        public Int32 CodAlmacen
        {
            get { return iCodAlmacen; }
            set { iCodAlmacen = value; }
        }
        public Int32 CodUsuario
        {
            get { return iCodUsuario; }
            set { iCodUsuario = value; }
        }
        public Int32 CodGrupo
        {
            get { return iCodGrupo; }
            set { iCodGrupo = value; }
        }
        public Int32 CodLinea
        {
            get { return iCodLinea; }
            set { iCodLinea = value; }
        }
        public Int32 CodFamilia
        {
            get { return iCodFamilia; }
            set { iCodFamilia = value; }
        }
        public Int32 CodMarca
        {
            get { return iCodMarca; }
            set { iCodMarca = value; }
        }
        public Int32 CodUnidadMedida
        {
            get { return iCodUnidadMedida; }
            set { iCodUnidadMedida = value; }
        }
        public Int32 CodTipoArticulo
        {
            get { return iCodTipoArticulo; }
            set { iCodTipoArticulo = value; }
        }
        public Int32 CodControlStock
        {
            get { return iCodControlStock; }
            set { iCodControlStock = value; }
        }
        public String Referencia
        {
            get { return sReferencia; }
            set { sReferencia = value; }
        }
        public Decimal PrecioCatalogo
        {
            get { return dPrecioCatalogo; }
            set { dPrecioCatalogo = value; }
        }
        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }
        public Boolean ConIgv
        {
            get { return bConIGV; }
            set { bConIGV = value; }
        }
        public Decimal ValorProm
        {
            get { return dValorProm; }
            set { dValorProm = value; }
        }
        public Decimal PrecioProm
        {
            get { return dPrecioProm; }
            set { dPrecioProm = value; }
        }
        public Decimal Recargo
        {
            get { return dRecargo; }
            set { dRecargo = value; }
        }
        public Decimal ValorVenta
        {
            get { return dValorVenta; }
            set { dValorVenta = value; }
        }
        public Decimal PrecioVenta
        {
            get { return dPrecioVenta; }
            set { dPrecioVenta = value; }
        }
        public Decimal PrecioVentaSoles
        {
            get { return dPrecioVentaSoles; }
            set { dPrecioVentaSoles = value; }
        }
        public Boolean Oferta
        {
            get { return bOferta; }
            set { bOferta = value; }
        }       
        public Decimal PDescuento
        {
            get { return dPDescuento; }
            set { dPDescuento = value; }
        }
        public Decimal MontoDscto
        {
            get { return dMontoDscto; }
            set { dMontoDscto = value; }
        }
        public Decimal PrecioOferta
        {
            get { return dPrecioOferta; }
            set { dPrecioOferta = value; }
        }
        public Boolean PrecioVariable
        {
            get { return bPrecioVariable; }
            set { bPrecioVariable = value; }
        }
        public Decimal MaximoDscto
        {
            get { return dMaximoDscto; }
            set { dMaximoDscto = value; }
        }
        public Decimal StockActual
        {
            get { return dStockActual; }
            set { dStockActual = value; }
        }
        public Decimal StockDisponible
        {
            get { return dStockDisponible; }
            set { dStockDisponible = value; }
        }
        public Decimal StockMinimo
        {
            get { return dStockMinimo; }
            set { dStockMinimo = value; }
        }
        public Decimal StockMaximo
        {
            get { return dStockMaximo; }
            set { dStockMaximo = value; }
        }
        public Decimal StockReposicion
        {
            get { return dStockReposicion; }
            set { dStockReposicion = value; }
        }
        public Decimal Soles
        {
            get { return dSoles; }
            set { dSoles = value; }
        }
        public Decimal TotalIngresos
        {
            get { return dTotalIngresos; }
            set { dTotalIngresos = value; }
        }
        public Decimal TotalSalidas
        {
            get { return dTotalSalidas; }
            set { dTotalSalidas = value; }
        }
        public Decimal TotalSolesIngresos
        {
            get { return dTotalSolesIngresos; }
            set { dTotalSolesIngresos = value; }
        }
        public Decimal TotalSolesSalidas
        {
            get { return dTotalSolesSalidas; }
            set { dTotalSolesSalidas = value; }
        }
        public Boolean Igv
        {
            get { return bIgv; }
            set { bIgv = value; }
        }
        public Boolean Detraccion
        {
            get { return bDetraccion; }
            set { bDetraccion = value; }
        }
        public Boolean Estado
        {
            get { return bEstado; }
            set { bEstado = value; }
        }
        public DateTime UltimaModificacion
        {
            get { return dtUltimaModificacion; }
            set { dtUltimaModificacion = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dtFechaRegistro; }
            set { dtFechaRegistro = value; }
        }
        public String UnidadDescrip
        {
            get { return sUnidaddescrip; }
            set { sUnidaddescrip = value; }
        }
        public Decimal Comision
        {
            get { return dComision; }
            set { dComision = value; }
        }

        public decimal ValorPromsoles
        {
            get { return dvalorPromsoles; }
            set { dvalorPromsoles = value; }
        }

        //Josué Joshua León Yalta
        public String Colorprimario
        {
            get { return scolorprimario; }
            set { scolorprimario = value; }
        }

        public String Marca
        {
            get { return smarca; }
            set { smarca = value; }
        }

        public String Modelo
        {
            get { return smodelo; }
            set { smodelo = value; }
        }

        public String Colorsecundario
        {
            get { return scolorsecundario; }
            set { scolorsecundario = value; }
        }

        public Int32 ContieneTalla
        {
            get { return iContieneTalla; }
            set { iContieneTalla = value; }
        }

        public List<clsTalla> Tallas { get => tallas; set => tallas = value; }


        #endregion propiedades


    }

    #region Entidad Fotografia
    public class clsEntFotografia
    {
        private Int32 codfotografia;
        private Int32 codrelacion;
        private Image fotografia;
        private Int32 codusuario;
        private Boolean estado;
        private DateTime fechareg;
        private Int32 icodAlamcen;

        public DateTime Fechareg
        {
            get { return fechareg; }
            set { fechareg = value; }
        }

        public Boolean Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public Int32 Codusuario
        {
            get { return codusuario; }
            set { codusuario = value; }
        }

        public Image Fotografia
        {
            get { return fotografia; }
            set { fotografia = value; }
        }

        public Int32 Codrelacion
        {
            get { return codrelacion; }
            set { codrelacion = value; }
        }

        public Int32 Codfotografia
        {
            get { return codfotografia; }
            set { codfotografia = value; }
        }

        public Int32 CodAlmacen
        {
            get { return icodAlamcen; }
            set { icodAlamcen = value; }
        }
    }
    #endregion
}
