﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsSerieProducto
    {
        #region propiedades

        private Int32 iCodSerieProducto;
        private String sReferencia;
        private String sDescripcion;
        private Int32 iEstado;
        private Int32 iTallamin;
        private Int32 iTallamax;
        private DateTime dtFecharegistro;
        private Int32 iTallaminI;
        private Int32 iTallamaxI;
        private Int32 icodUser;

        public Int32 CodUser
        {
            get { return icodUser; }
            set { icodUser = value; }
        }

        public Int32 TallamaxI
        {
            get { return iTallamaxI; }
            set { iTallamaxI = value; }
        }

        public Int32 TallaminI
        {
            get { return iTallaminI; }
            set { iTallaminI = value; }
        }

        public DateTime Fecharegistro
        {
            get { return dtFecharegistro; }
            set { dtFecharegistro = value; }
        }

        public Int32 Tallamax
        {
            get { return iTallamax; }
            set { iTallamax = value; }
        }

        public Int32 Tallamin
        {
            get { return iTallamin; }
            set { iTallamin = value; }
        }

        public Int32 Estado
        {
            get { return iEstado; }
            set { iEstado = value; }
        }

        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }

        public String Referencia
        {
            get { return sReferencia; }
            set { sReferencia = value; }
        }

        public Int32 CodSerieProducto
        {
            get { return iCodSerieProducto; }
            set { iCodSerieProducto = value; }
        }

        #endregion propiedades

    }
}
