﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsTaco
    {
        Int32 iCodTaco;
        String iReferencia;
        String iDescripcion;
        Int32 iEstado;
        DateTime iFechaRegistro;

        public Int32 CodTaco
        {
            get { return iCodTaco; }
            set { iCodTaco = value; }
        }

        public String Referencia
        {
            get { return iReferencia; }
            set { iReferencia = value; }
        }

        public String Descripcion
        {
            get { return iDescripcion; }
            set { iDescripcion = value; }
        }

        public Int32 Estado
        {
            get { return iEstado; }
            set { iEstado = value; }
        }

        public DateTime FechaRegistro
        {
            get { return iFechaRegistro; }
            set { iFechaRegistro = value; }
        }

    }
}
