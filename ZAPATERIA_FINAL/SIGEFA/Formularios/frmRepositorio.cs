﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.SunatFacElec;
using SIGEFA.SunatFacElect;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinApp.Comun.Dto.Intercambio;

namespace SIGEFA.Formularios
{
    public partial class frmRepositorio : Form
    {
        clsAdmRepositorio AdmRepo = new clsAdmRepositorio();
        clsAdmTipoDocumento Admtd = new clsAdmTipoDocumento();
        clsAdmSerie Admserie = new clsAdmSerie();
        Facturacion facturacion = new Facturacion();
        clsEmpresa empresa = new clsEmpresa();
        clsAdmEmpresa Admempresa = new clsAdmEmpresa();
        Herramientas herramienta = new Herramientas();
        int estado = 0;

        public frmRepositorio()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            cargaRepositorio();
        }

        private void frmRepositorio_Load(object sender, EventArgs e)
        {
            cbTipocomprobante.DataSource = Admtd.CargaTipoDocumento_repo();
            cbTipocomprobante.DisplayMember = "descripcion";
            cbTipocomprobante.ValueMember = "codTipoDocumento";
            cbTipocomprobante.SelectedIndex = 0;

            cbSerie.DataSource = Admserie.MuestraSeries(Convert.ToInt32(cbTipocomprobante.SelectedValue), frmLogin.iCodAlmacen);
            cbSerie.DisplayMember = "serie";
            cbSerie.ValueMember = "codSerie";
            cbTipocomprobante.SelectedIndex = 0;

            var estados = new BindingList<KeyValuePair<string, string>>();

            estados.Add(new KeyValuePair<string, string>("-1", "No Enviado"));
            estados.Add(new KeyValuePair<string, string>("0", "Enviado"));
            estados.Add(new KeyValuePair<string, string>("2", "Todos"));

            cbEstadodoc.DataSource = estados;
            cbEstadodoc.ValueMember = "Key";
            cbEstadodoc.DisplayMember = "Value";
            cbEstadodoc.SelectedIndex = 0;

            dgv_repositorio.AutoGenerateColumns = false;

            cbFinicio.Value = DateTime.Now;
            cbFfin.Value = DateTime.Now;

        }

        private void cargaRepositorio()
        {
            try
            {
                DateTime finicio = cbFinicio.Value;
                DateTime ffin = cbFfin.Value;
                var idtipocomprobante = cbTipocomprobante.SelectedValue;
                clsSerie serie = new clsSerie();
                serie.Tipodocumento = new clsTipoDocumento();
                serie.Tipodocumento.CodTipoDocumento = Convert.ToInt32(idtipocomprobante);
                serie.Serie = cbSerie.GetItemText(cbSerie.SelectedItem);
                int estado = Convert.ToInt32(cbEstadodoc.SelectedValue);



                dgv_repositorio.DataSource = AdmRepo.listar_repositorio_xtsfe(serie, finicio, ffin,estado);

                //frmLogin.iCodAlmacen

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void cbTipocomprobante_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnEnviarrepo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv_repositorio.RowCount > 0)
                {

                    if (cbEstadodoc.SelectedIndex == 0)
                    {
                        enviar_documento();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private async void enviar_documento()
        {
            int enviados = 0;
            clsArchivo archivo = null;
            int todocorrecto = 0;
            clsRepositorio r = new clsRepositorio();
            int total = dgv_repositorio.Rows.Count;
            try
            {
                foreach (DataGridViewRow row in dgv_repositorio.Rows)
                {
                    empresa = Admempresa.CargaEmpresa(
                            int.Parse(dgv_repositorio.Rows[row.Index].Cells[idEmpresa.Index].Value.ToString())
                        );

                    r = AdmRepo.listar_xidrepositorio(int.Parse(dgv_repositorio.Rows[row.Index].Cells[idrepositorio.Index].Value.ToString()));

                    if(empresa == null)
                    {
                        MessageBox.Show("No se ha podido cargar empresa...", "Advertencia");
                        return;
                    }

                    //if (!archivo_existe(row.Cells[rutadocxml.Index].Value.ToString()))
                    //{
                    //descargar_xml(row.Index, 0);
                    //descargar_pdf(row.Index, 0);
                    //}

                    var tramaXmlSinFirma = (File.Exists(dgv_repositorio.Rows[row.Index].Cells[rutaxml.Index].Value.ToString())) ?
                                                Convert.ToBase64String(File.ReadAllBytes(row.Cells[rutaxml.Index].Value.ToString())) :
                                                Convert.ToBase64String(File.ReadAllBytes(herramienta.GetResourcesPath5() + "\\" + dgv_repositorio.Rows[row.Index].Cells[nombredocxml.Index].Value.ToString()));

                    string iddocumento = dgv_repositorio.Rows[row.Index].Cells[comprobante.Index].Value.ToString();
                    string tipodocumento = Admtd.CargaTipoDocumento(Convert.ToInt32(dgv_repositorio.Rows[row.Index].Cells[idtipocomprobante.Index].Value.ToString())).Codsunat;
                    int idrepo= Convert.ToInt32(dgv_repositorio.Rows[row.Index].Cells[idrepositorio.Index].Value);

                    var firmadoRequest = new SunatFacElect.FirmadoRequest
                    {
                        TramaXmlSinFirma = tramaXmlSinFirma,
                        CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(Program.Certificado + "\\" + empresa.Nombrecertificado)),
                        PasswordCertificado = empresa.Clavecertificado,
                        UnSoloNodoExtension = false
                    };

                    FirmarController enviar = new FirmarController();

                    var respuestaFirmado = enviar.FirmadoResponse(firmadoRequest);

                    if (!respuestaFirmado.Exito)
                        throw new ApplicationException(respuestaFirmado.MensajeError);

                    await facturacion.Enviar(empresa, iddocumento, tipodocumento, respuestaFirmado.TramaXmlFirmado);

                    EnviarDocumentoResponse rpta = facturacion.rpta;

                    rpta = facturacion.rpta;

                    if (rpta != null)
                    {

                        if (rpta.CodigoRespuesta == "0" && rpta.TramaZipCdr != null)
                        {

                            r.Estadosunat = 0;
                            r.Mensajesunat = rpta.MensajeRespuesta;
                            r.ICodUser = frmLogin.iCodUser;

                            String rutazip = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CDR\\" + "R-" + empresa.Ruc + "-" + tipodocumento + "-" + iddocumento + ".zip";
                            File.WriteAllBytes(rutazip, Convert.FromBase64String(rpta.TramaZipCdr));

                            File.WriteAllBytes($"{Program.CarpetaCdr}\\{"R-" + empresa.Ruc + "-" + tipodocumento + "-" + iddocumento + ".zip"}",
                            Convert.FromBase64String(rpta.TramaZipCdr));

                            /*
							* preguntar si el zip se encuentra en la ruta
							*/

                            if (File.Exists(rutazip))
                            {
                                archivo = new clsArchivo()
                                {
                                    Xml = File.ReadAllBytes(dgv_repositorio.Rows[row.Index].Cells[rutaxml.Index].Value.ToString()),
                                    Zip = File.ReadAllBytes(rutazip)
                                };

                                r.Archivo = archivo;
                            }
                            else
                            {
                                MessageBox.Show("SUNAT no ha devuelto la constancia de recepción\n " +
                                                "Por favor verificar archivo en el módulo de\n CONSULTA CDR ");
                            }

                            if (AdmRepo.actualizar_repositorio(r) != 0)
                            {
                                enviados++;
                            }
                        }
                        else
                        {
                            r.Mensajesunat = rpta.MensajeError;

                            /*
							* condicion agregada debido a que codigoRespuesta es nulo
							*/

                            if (rpta.CodigoRespuesta == "1033" || rpta.MensajeError.Contains("1033"))
                            {

                                r.Estadosunat = 0;
                                AdmRepo.actualizar_repositorio(r);

                                MessageBox.Show("El documento fue registro previamente con otros datos\n " +
                                                "Por favor verificar en SUNAT la existencia del mismo\n " +
                                                "Doc. a buscar : " + dgv_repositorio.Rows[row.Index].Cells[comprobante.Index].Value);
                            }
                            else
                            {
                                r.Estadosunat = -1;

                                AdmRepo.actualizar_repositorio(r);
                                MessageBox.Show(rpta.MensajeError + " - " + rpta.MensajeRespuesta);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error de envio...");
                    }
                        

                }
                    
                    if (enviados==total)
                    {
                        MessageBox.Show("Los documentos fueron enviados de forma correcta");
                    }
                    else
                    {
                        if (enviados < total)
                        {
                            MessageBox.Show("No todos los documentos fueron enviados de forma correcta");
                        }

                        if (enviados == 0)
                        {
                            MessageBox.Show("Se produjo algun error...no se envio ningun archivo");
                        }   
                    }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbEstadodoc_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //estado =Convert.ToInt32(cbEstadodoc.SelectedItem);
        }
    }
}
