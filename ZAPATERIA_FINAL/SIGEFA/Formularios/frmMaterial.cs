﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
    public partial class frmMaterial : Form
    {
        clsAdmCorte admcorte = new clsAdmCorte();
        public clsCorte corte = new clsCorte();
        public Int32 Proceso = 0; //(1) Nuevo (2)Editar (3)CONSULTA F1
        public Int32 Procedencia = 0; //(1)Nota Ingreso (2)Nota Salida 

        clsValidar ok = new clsValidar();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;

        public frmMaterial()
        {
            InitializeComponent();
        }

        private void frmMaterial_Load(object sender, EventArgs e)
        {
            CargaLista();
            label2.Text = "Descripcion";
            label3.Text = "descripcion";
            if (Proceso == 3)
            {
                bloquearbotones();
            }
        }

        private void bloquearbotones()
        {
            btnNuevo.Visible = false;
            btnEditar.Visible = false;
            btnEliminar.Visible = false;
            btnGuardar.Text = "Aceptar";
            //btnGuardar.ImageIndex = 6;
        }

        private void CargaLista()
        {
            dgvCorte.DataSource = data;
            data.DataSource = admcorte.MuestraCorte();
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvCorte.ClearSelection();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvCorte.CurrentRow.Index != -1 && corte.CodCorte != 0)
            {
                DialogResult dlgResult = MessageBox.Show("Esta seguro que desea eliminar los datos definitivamente", "Colores", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    if (admcorte.deleteCorte(corte.CodCorte))
                    {
                        MessageBox.Show("Los datos han sido eliminado correctamente", "Colores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CargaLista();
                    }
                }
            }
        }

        private void CambiarEstados(Boolean Estado)
        {
            groupBox1.Visible = Estado;
            groupBox3.Visible = !Estado;
            btnGuardar.Enabled = !Estado;
            btnNuevo.Enabled = Estado;
            btnEditar.Enabled = Estado;
            btnEliminar.Enabled = Estado;
            txtCodigo.Text = "";
            txtDescripcion.Text = "";
            txtReferencia.Text = "";
            superValidator1.Validate();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (btnGuardar.Text == "Guardar")
            {
                if (superValidator1.Validate())
                {
                    if (Proceso != 0 && txtDescripcion.Text != "")
                    {
                        corte.Descripcion = txtDescripcion.Text;
                        corte.CodUser = frmLogin.iCodUser;
                        corte.Referencia = txtReferencia.Text;
                        if (Proceso == 1)
                        {
                            if (admcorte.insertCorte(corte))
                            {
                                MessageBox.Show("Los datos se guardaron correctamente", "Moneda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CambiarEstados(true);
                                CargaLista();
                            }
                        }
                        else if (Proceso == 2)
                        {
                            if (admcorte.insertCorte(corte))
                            {
                                MessageBox.Show("Los datos se guardaron correctamente", "Moneda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CambiarEstados(true);
                                CargaLista();
                            }
                        }
                        Proceso = 0;
                    }
                }
            }
            else if (btnGuardar.Text == "Aceptar")
            {
                if (Proceso == 3)
                {
                    this.Close();
                }
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            groupBox3.Text = "Registro Nuevo";
            Proceso = 1;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            groupBox3.Text = "Editar Registro";
            Proceso = 2;
            txtCodigo.Text = corte.CodCorte.ToString();
            txtDescripcion.Text = corte.Descripcion;
            txtReferencia.Text = corte.Referencia;
        }

        private void dgvCorte_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                corte.CodCorte = Convert.ToInt32(dgvCorte.Rows[dgvCorte.CurrentCell.RowIndex].Cells[CODCORTE.Name].Value);
                corte.Descripcion = dgvCorte.Rows[dgvCorte.CurrentCell.RowIndex].Cells[DESCRIPCION.Name].Value.ToString();
                corte.Referencia = dgvCorte.Rows[dgvCorte.CurrentCell.RowIndex].Cells[referencia.Name].Value.ToString();
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
                if (Proceso == 3)
                {
                    btnGuardar.Enabled = true;
                }
            }
            else if (dgvCorte.SelectedRows.Count == 0)
            {
                btnEditar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltro.Text.Length >= 2)
                {
                    data.Filter = String.Format("[{0}] like '*{1}*'", label2.Text.Trim(), txtFiltro.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void frmMaterial_Shown(object sender, EventArgs e)
        {
            CargaLista();
            txtFiltro.Focus();
        }

        private void dgvCorte_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvCorte.Rows.Count >= 1 && e.Row.Selected)
            {
                corte.CodCorte = Convert.ToInt32(e.Row.Cells[CODCORTE.Name].Value);
                corte.Descripcion = e.Row.Cells[DESCRIPCION.Name].Value.ToString();
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
                if (Proceso == 3)
                {
                    btnGuardar.Enabled = true;
                }
            }
            else if (dgvCorte.SelectedRows.Count == 0)
            {
                btnEditar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }

        private void dgvCorte_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            label2.Text = dgvCorte.Columns[e.ColumnIndex].HeaderText;
            label3.Text = dgvCorte.Columns[e.ColumnIndex].DataPropertyName;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (groupBox1.Visible)
            {
                this.Close();
            }
            else
            {
                Proceso = 0;
                CambiarEstados(true);
            }
        }
    }
}
