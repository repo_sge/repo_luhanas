using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmTaco : DevComponents.DotNetBar.OfficeForm
    {
        clsAdmTaco admtaco = new clsAdmTaco();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;
        clsTaco taco = new clsTaco();
        Int32 Proceso = 0;
        public frmTaco()
        {
            InitializeComponent();
        }

        private void frmTaco_Load(object sender, EventArgs e)
        {
            CargaLista();
            label2.Text = "Codigo";
            label3.Text = "Descripcion";
        }

        private void CargaLista()
        {
            dgvTaco.DataSource = data;
            dgvTaco.DataSource = admtaco.MuestraTaco();
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvTaco.ClearSelection();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            groupBox2.Text = "Registro Nuevo";
            Proceso = 1;
        }

        private void CambiarEstados(Boolean Estado)
        {
            groupBox1.Visible = Estado;
            groupBox2.Visible = !Estado;
            btnGuardar.Enabled = !Estado;
            btnNuevo.Enabled = Estado;
            btnEditar.Enabled = Estado;
            btnEliminar.Enabled = Estado;
            btnReporte.Enabled = Estado;
            txtCodigo.Text = "";
            txtDescripcion.Text = "";
            superValidator1.Validate();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (superValidator1.Validate())
            {
                if (Proceso != 0 && txtDescripcion.Text != "")
                {
                    taco.Descripcion = txtDescripcion.Text;
                    if (Proceso == 1)
                    {
                        if (admtaco.insert(taco))
                        {
                            MessageBox.Show("Los datos se guardaron correctamente", "Gestion Familia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            CambiarEstados(true);
                            CargaLista();
                        }
                    }
                    else
                    {
                        if (admtaco.update(taco))
                        {
                            MessageBox.Show("Los datos se guardaron correctamente", "Gestion Familia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            CambiarEstados(true);
                            CargaLista();
                        }
                    }

                    Proceso = 0;
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            Proceso = 2;
            groupBox2.Text = "Editar Registro";
            txtCodigo.Text = taco.CodTaco.ToString();
            txtDescripcion.Text = taco.Descripcion;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (groupBox1.Visible)
            {
                this.Close();
            }
            else
            {
                Proceso = 0;
                CambiarEstados(true);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
            if (dgvTaco.CurrentRow.Index != -1 && taco.CodTaco != 0)
            {
                DialogResult dlgResult = MessageBox.Show("Esta seguro que desea eliminar los datos definitivamente", "Marcas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    if (admtaco.delete(taco.CodTaco))
                    {
                        MessageBox.Show("Los datos han sido eliminado correctamente", "Marcas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CargaLista();
                    }
                }
            }
        }

        private void dgvTaco_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if(dgvTaco.Rows.Count >= 1 && e.Row.Selected)
            {
                taco.CodTaco = Convert.ToInt32(e.Row.Cells[codigo.Name].Value);
                taco.Descripcion = Convert.ToString(e.Row.Cells[descripcion.Name].Value);
                taco.FechaRegistro = Convert.ToDateTime(e.Row.Cells[fecha.Name].Value);
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else if (dgvTaco.SelectedRows.Count == 0)
            {
                btnEditar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltro.Text.Length >= 2)
                {
                    data.Filter = String.Format("[{0}] like '*{1}*'", label3.Text.Trim(), txtFiltro.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void dgvTaco_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {            
            label2.Text = dgvTaco.Columns[e.ColumnIndex].HeaderText;
            label3.Text = dgvTaco.Columns[e.ColumnIndex].DataPropertyName;
            txtFiltro.Focus();
        }
    }
}