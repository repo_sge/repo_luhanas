﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.Conexion;
using System.Data;
using MySql.Data.MySqlClient;
using SIGEFA.Entidades;

namespace SIGEFA.InterMySql
{
    class MysqlCuello : ICuello
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        #region Implementacion IFamilias

        public Boolean Insert(clsCuello cue)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaCuello", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("descripcion", cue.Descripcion);
                oParam = cmd.Parameters.AddWithValue("codusu", cue.CodUser);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                cue.CodCuelloNuevo = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Update(clsCuello cue)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaCuello", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codcuello", cue.CodCuello);
                cmd.Parameters.AddWithValue("descripcion", cue.Descripcion);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Delete(Int32 CodCuello)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarCuello", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codcuello", CodCuello);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsCuello CargaCuello(Int32 Codigo)
        {
            clsCuello mar = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraCuello", con.conector);
                cmd.Parameters.AddWithValue("codmar", Codigo);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mar = new clsCuello();
                        mar.CodCuello = Convert.ToInt32(dr.GetDecimal(0));
                        mar.Descripcion = dr.GetString(1);
                        mar.Estado = Convert.ToInt32(dr.GetBoolean(2));
                        mar.CodUser = Convert.ToInt32(dr.GetDecimal(3));
                        mar.FechaRegistro = dr.GetDateTime(4);// capturo la fecha 
                    }

                }
                return mar;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaCuello()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaCuello", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
        #endregion
    }
}
