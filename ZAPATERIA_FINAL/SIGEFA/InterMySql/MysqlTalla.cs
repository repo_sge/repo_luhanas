﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using SIGEFA.Entidades;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
namespace SIGEFA.InterMySql
{
    class MysqlTalla : ITalla
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public clsTalla CargaTallaDes(String talla)
        {
            clsTalla tal = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("ListaTallaValor", con.conector);
                cmd.Parameters.AddWithValue("val", talla);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tal = new clsTalla();

                        tal.codTalla = dr.GetInt32(0);
                        tal.Nombre = dr.GetString(1);
                        tal.Corto = dr.GetString(2);
                        tal.CodLinea = dr.GetInt32(3);
                        tal.Valor = dr.GetString(4);
                    }

                }
                return tal;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public clsTalla CargaTallaCod(Int32 cod)
        {
            clsTalla tal = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listTallaxid", con.conector);
                cmd.Parameters.AddWithValue("_idtalla", cod);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tal = new clsTalla();
                        tal.codTalla = dr.GetInt32(0);
                        tal.Nombre = dr.GetString(1);
                        tal.Corto = "";
                        tal.CodLinea = dr.GetInt32(3);
                        tal.Valor = dr.GetString(4);
                        tal.Estado = dr.GetInt32(5);
                        tal.CodLinea = dr.GetInt32(6);
                    }

                }
                return tal;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable ConsultaTallaCompra(Decimal tallamin, Decimal tallamax, Int32 tipot, Int32 line)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ConsultaTallaCompra", con.conector);
                cmd.Parameters.AddWithValue("tal1", tallamin);
                cmd.Parameters.AddWithValue("tal2", tallamax);
                cmd.Parameters.AddWithValue("codtipo", tipot);
                cmd.Parameters.AddWithValue("line", line);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }

        
        
        }

        public DataTable MuestraTallaOC()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaTallaOC", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        #region Nuevos Cambios


        public DataTable ListaTallaporLinea(Int32 codline, Int32 min, Int32 max, Int32 tipot)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaTallaporLinea", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("tmin", min);
                cmd.Parameters.AddWithValue("tmax", max);
                cmd.Parameters.AddWithValue("ctipota", tipot);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsTalla DevolverCodTalla(Int32 nom)
        {
            clsTalla tal = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("DevolverCodTalla", con.conector);
                cmd.Parameters.AddWithValue("nom", nom);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tal = new clsTalla();
                        tal.codTalla = dr.GetInt32(0);
                        tal.Nombre = dr.GetString(1);                        
                        tal.CodLinea = dr.GetInt32(3);
                        tal.Valor = dr.GetString(4);
                    }
                }
                return tal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsTalla DevolverCodTalla(String nom, Int32 tipotalla)
        {
            clsTalla tal = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("DevolverCodTalla1", con.conector);
                cmd.Parameters.AddWithValue("nomb", nom);
                cmd.Parameters.AddWithValue("tipo", tipotalla);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tal = new clsTalla();
                        tal.codTalla = dr.GetInt32(0);
                        tal.Nombre = dr.GetString(1);
                        tal.CodLinea = dr.GetInt32(3);
                        tal.Valor = dr.GetString(4);
                    }
                }
                return tal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        #endregion

      

    }
}
