﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.Conexion;
using MySql.Data.MySqlClient;
using System.Data;
using SIGEFA.Formularios;

namespace SIGEFA.InterMySql
{
    class MysqlVentasMes : IVentasMes
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public Int32 BuscarMes(Int32 Year)
        {
            
            try
            {
                Int32 mespasado = 0;
                con.conectarBD();

                cmd = new MySqlCommand("BuscarUltimoMes", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("annio", Year);
                oParam = cmd.Parameters.AddWithValue("cod_alma", frmLogin.iCodAlmacen);
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mespasado = Convert.ToInt32(dr.GetInt32(0));
                    }
                }
                return mespasado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean GuardaVentas(Int32 mespasado, Int32 anio, Int32 codalmacen, Int32 mesactual, Int32 dia)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("GuardaVentasxMes", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("annio", anio);
                oParam = cmd.Parameters.AddWithValue("cod_alma", codalmacen);
                oParam = cmd.Parameters.AddWithValue("mespasado", mespasado);
                oParam = cmd.Parameters.AddWithValue("mesactual", mesactual);
                oParam = cmd.Parameters.AddWithValue("diactual", dia);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable MostrarVentasMes(Int32 mes, Int32 anio, Int32 codalmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("MostrarVentasMes", con.conector);
                cmd.Parameters.AddWithValue("mesactual", mes);
                cmd.Parameters.AddWithValue("annio", anio);
                cmd.Parameters.AddWithValue("codalma", codalmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
