﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using System;
using System.Data;
using System.Windows.Forms;

namespace SIGEFA.InterMySql
{
    internal class MysqlEmpresa : IEmpresa
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private DataTable tabla = null;

        #region Implementacion IEmpresa

        public Boolean Insert(clsEmpresa emp)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaEmpresa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("razonsocial", emp.RazonSocial);
                oParam = cmd.Parameters.AddWithValue("ruc", emp.Ruc);
                oParam = cmd.Parameters.AddWithValue("direccion", emp.Direccion);
                oParam = cmd.Parameters.AddWithValue("telefono", emp.Telefono);
                oParam = cmd.Parameters.AddWithValue("_usuariosol", emp.Usuariosol);
                oParam = cmd.Parameters.AddWithValue("_clavesol", emp.Clavesol);
                oParam = cmd.Parameters.AddWithValue("_nombrecer", emp.Nombrecertificado);
                oParam = cmd.Parameters.AddWithValue("_clavecer", emp.Clavecertificado);
                oParam = cmd.Parameters.AddWithValue("_urlenvio", emp.Urlenvio);
                oParam = cmd.Parameters.AddWithValue("estado", emp.Estado);
                oParam = cmd.Parameters.AddWithValue("codusu", emp.CodUser);
                oParam = cmd.Parameters.AddWithValue("_departamento", emp.Departamento);
                oParam = cmd.Parameters.AddWithValue("_provincia", emp.Provincia);
                oParam = cmd.Parameters.AddWithValue("_distrito", emp.Distrito);
                oParam = cmd.Parameters.AddWithValue("_ubigeo", emp.Ubigeo);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                emp.CodEmpresaNuevo = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Update(clsEmpresa emp)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaEmpresa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codemp", emp.CodEmpresa);

                cmd.Parameters.AddWithValue("razonsocial", emp.RazonSocial);
                cmd.Parameters.AddWithValue("ruc", emp.Ruc);
                cmd.Parameters.AddWithValue("direccion", emp.Direccion);
                cmd.Parameters.AddWithValue("telefono", emp.Telefono);
                cmd.Parameters.AddWithValue("_usuariosol", emp.Usuariosol);
                cmd.Parameters.AddWithValue("_clavesol", emp.Clavesol);
                cmd.Parameters.AddWithValue("_nombrecer", emp.Nombrecertificado);
                cmd.Parameters.AddWithValue("_clavecer", emp.Clavecertificado);
                cmd.Parameters.AddWithValue("_urlenvio", emp.Urlenvio);
                cmd.Parameters.AddWithValue("_departamento", emp.Departamento);
                cmd.Parameters.AddWithValue("_provincia", emp.Provincia);
                cmd.Parameters.AddWithValue("_distrito", emp.Distrito);
                cmd.Parameters.AddWithValue("_ubigeo", emp.Ubigeo);
                cmd.Parameters.AddWithValue("estado", emp.Estado);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Delete(Int32 CodEmpresa)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarEmpresa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codemp", CodEmpresa);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsEmpresa CargaEmpresa(Int32 Codigo)
        {
            clsEmpresa emp = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraEmpresa", con.conector);
                cmd.Parameters.AddWithValue("codempre", Codigo);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        emp = new clsEmpresa();
                        emp.CodEmpresa = Convert.ToInt32(dr.GetDecimal(0));
                        emp.RazonSocial = dr.GetString(1);
                        emp.Ruc = dr.GetString(2);
                        emp.Direccion = dr.GetString(3);
                        emp.Telefono = dr.GetString(4);
                        emp.Departamento = dr.GetString(5);
                        emp.Provincia = dr.GetString(6);
                        emp.Distrito = dr.GetString(7);
                        emp.Ubigeo = dr.GetString(8);
                        emp.Estado = dr.GetBoolean(9);
                        emp.FechaRegistro = dr.GetDateTime(10);// capturo la fecha
                        emp.Urlenvio = dr.GetString(11);
                        emp.Nombrecertificado = dr.GetString(12);
                        emp.Clavecertificado = dr.GetString(13);
                        emp.Usuariosol = dr.GetString(14);
                        emp.Clavesol = dr.GetString(15);
                    }
                }
                return emp;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean VerificaRUC(String RUC)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("VerificaRUC", con.conector);
                cmd.Parameters.AddWithValue("rucingresado", RUC);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaEmpresas()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaEmpresas", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable BuscaEmpresas(Int32 Criterio, String Filtro)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("FiltraEmpresa", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@criterio", Criterio);
                cmd.Parameters.AddWithValue("@filtro", Filtro);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable CargaEmpresas()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaEmpresas", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean UpdateConfiguracion(clsParametros Config)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaConfiguracion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("config", Config.CodConfiguracion);
                cmd.Parameters.AddWithValue("igv", Config.IGV);
                cmd.Parameters.AddWithValue("dias", Config.DiasVigencia);
                cmd.Parameters.AddWithValue("codusu", Config.CodUser);
                cmd.Parameters.AddWithValue("venfac", Config.FacturasVencidas);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsParametros CargaConfiguracion()
        {
            clsParametros config = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("CargaConfiguracion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        config = new clsParametros();
                        config.CodConfiguracion = Convert.ToInt32(dr.GetDecimal(0));
                        config.IGV = Convert.ToDecimal(dr.GetDecimal(1));
                        config.DiasVigencia = Convert.ToInt32(dr.GetDecimal(2));
                        config.Estado = dr.GetBoolean(3);
                        config.CodUser = Convert.ToInt32(dr.GetDecimal(4));
                        config.FechaRegistro = dr.GetDateTime(5);// capturo la fecha
                        config.FacturasVencidas = dr.GetBoolean(6);
                    }
                }
                return config;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsEmpresa listar_empresa_xestado()
        {
            clsEmpresa empre = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_empresa_xestado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        empre = new clsEmpresa()
                        {
                            CodEmpresa = (int)dr["codEmpresa"],
                            RazonSocial = (string)dr["razonsocial"],
                            Tipodocidentidad = new clsTipoDocumentoIdentidad()
                            {
                                Idtipodocumentoidentidad = (int)dr["idtipodocumentoidentidad"],
                                Codsunat = (string)dr["codsunat"]
                            },
                            Ruc = (string)dr["ruc"],
                            Urlenvio = (string)dr["urlenvio"],
                            Ubigeo = (string)dr["ubigeo"],
                            Direccion = (string)dr["direccion"],
                            Departamento = (string)dr["departamento"],
                            Provincia = (string)dr["provincia"],
                            Distrito = (string)dr["distrito"],
                            Usuariosol = (string)dr["usuariosol"],
                            Clavesol = (string)dr["clavesol"],
                            Nombrecertificado = (string)dr["nombrecertificado"],
                            Clavecertificado = (string)dr["clavecertificado"],
                            Estado = ((int)dr["estado"]) == 1 ? true : false
                        };
                    }
                    dr.Close();
                }
                return empre;
            }
            catch (MySqlException ex)
            {
                return empre;
                MessageBox.Show("Error mysql..."+ex.Message.ToString());
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsEmpresa MuestraEmpresa_sucursal_almacen(int codSucursal, int codAlmacen)
        {
            clsEmpresa emp = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraEmpresa_sucursal_almacen", con.conector);
                cmd.Parameters.AddWithValue("_idsucursal", codSucursal);
                cmd.Parameters.AddWithValue("_idalmacen", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        emp = new clsEmpresa();
                        emp.CodEmpresa = Convert.ToInt32(dr.GetDecimal(0));
                        emp.RazonSocial = dr.GetString(1);
                        emp.Ruc = dr.GetString(2);
                        emp.Direccion = dr.GetString(3);
                        emp.Telefono = dr.GetString(4);
                        emp.Fax = dr.GetString(5);
                        emp.Representante = dr.GetString(6);
                        emp.Estado = dr.GetBoolean(7);
                        emp.FechaRegistro = dr.GetDateTime(8);// capturo la fecha
                        emp.Urlenvio = dr.GetString(9);
                        emp.Nombrecertificado = dr.GetString(10);
                        emp.Clavecertificado = dr.GetString(11);
                        emp.Usuariosol = dr.GetString(12);
                        emp.Clavesol = dr.GetString(13);
                        emp.Departamento= dr.GetString(14);
                        emp.Provincia= dr.GetString(15);
                        emp.Distrito= dr.GetString(16);
                        emp.Ubigeo= dr.GetString(17);
                        emp.Tipodocidentidad = new clsTipoDocumentoIdentidad();
                        emp.Tipodocidentidad.Codsunat = dr.GetInt32(18).ToString();
                    }
                }
                return emp;
            }
            catch (MySqlException ex)
            {
                throw;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        #endregion Implementacion IEmpresa
    }
}