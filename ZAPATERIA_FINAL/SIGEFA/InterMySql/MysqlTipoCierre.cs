﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Conexion;
using MySql.Data.MySqlClient;
using System.Data;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;

namespace SIGEFA.InterMySql
{
    class MysqlTipoCierre:ITipoCierre
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public Boolean Insert(clsTipoCierre cie)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("GuardaTipoCierre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("descripcion", cie.Descripcion);
                oParam = cmd.Parameters.AddWithValue("codusu", cie.CodUsuario);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();
                cie.CodTipoCierre = Convert.ToInt32(cmd.Parameters["newid"].Value);
                con.conectarBD();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public Boolean Update(clsTipoCierre cie)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaTipoCierre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codcie", cie.CodTipoCierre);
                cmd.Parameters.AddWithValue("descripcion", cie.Descripcion);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsTipoCierre CargaTipoCierre(Int32 Codigo)
        {
            clsTipoCierre tip = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraTipoCierre", con.conector);
                cmd.Parameters.AddWithValue("codcie", Codigo);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tip = new clsTipoCierre();
                        tip.CodTipoCierre = Convert.ToInt32(dr.GetDecimal(0));
                        tip.Descripcion = dr.GetString(1);
                        tip.estado = dr.GetBoolean(2);
                        tip.CodUsuario = Convert.ToInt32(dr.GetDecimal(3));
                        tip.FechaRegistro = dr.GetDateTime(4);// capturo la fecha 
                    }

                }
                return tip;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Delete(Int32 CodTipoCierre)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarTipoCierre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codTipoCierre", CodTipoCierre);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaTipoCierre()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaTipoCierre", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
           
        }
    }
}
