﻿using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmTipoImpuesto
    {
        private ITipoImpuesto MTimpuesto = new MysqlTipoImpuesto();

        public List<clsTipoImpuesto> listar_tipoimpuesto_xestado()
        {
            try
            {
                return MTimpuesto.listar_tipoimpuesto_xestado();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsTipoImpuesto listar_tipoimpuesto_xid(int id)
        {
            try
            {
                return MTimpuesto.listar_tipoimpuesto_xid(id);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
