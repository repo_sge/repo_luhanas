﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;

namespace SIGEFA.Administradores
{
    class clsAdmArqueoFondoFijo
    {
        IArqueoFondoFijo Marqueo = new MysqlArqueFondoFijo();
        public Boolean insert(clsArqueoFondoFijo arqe)
        {
            try
            {
                return Marqueo.Insert(arqe);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia");

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia");
                return false;
            }
        }

        public Boolean insertDetalle(clsDetalleArqueFondoFijo det)
        {
            try
            {
                return Marqueo.insertDetalle(det);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia");

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia");
                return false;
            }
        }
    }
}
