﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmTemporada
    {
        ITemporada Ttemp = new MysqlTemporada();

        public DataTable MuestraTemporada()
        {
            try
            {
                return Ttemp.ListaTemporadas();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
